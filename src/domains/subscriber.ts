interface Tier {
  name: string
}

interface Subscriber {
  id: number
  name: string
  subscription_date: string
  tier: Tier
}

interface SubscriberResponse {
  creator: {
    subscribers: Subscriber[]
  }
}

export type { Subscriber, SubscriberResponse }
