interface Creator {
  id: number
  name: string
  headline: string
  photo_url: string
  background_url: string
}

interface CreatoResponse {
  creator: Creator
}

export type { Creator, CreatoResponse }
