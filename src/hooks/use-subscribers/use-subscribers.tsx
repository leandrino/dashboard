import { useCallback, useState } from 'react'
import { SubscriberResponse } from '../../domains/subscriber';

const useSubscribers = () => {
  const [pending, setPending] = useState(false);
  const [value, setValue] = useState<SubscriberResponse | null>(null);
  const [error, setError] = useState(null);
  const [page, setPage] = useState(1);

  const handleExecute = useCallback(() => {
    setPending(true);
    setValue(null);
    setError(null);
    return fetch(`https://livus-frontend-test.s3.amazonaws.com/subscribers_${page}.json`)
      .then(async response => setValue(await response.json()))
      .catch(error => setError(error))
      .finally(() => setPending(false));
  }, [page]);

  const changePage = useCallback((pageNumber) => {
    setPage(pageNumber)
    handleExecute()
  }, [handleExecute])

  return { handleExecute, pending, value, error, changePage, page };
}

export { useSubscribers }
