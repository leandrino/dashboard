import { useCallback, useState } from 'react'
import { CreatoResponse } from '../../domains/creator';

const useCreator = () => {
  const [pending, setPending] = useState(false);
  const [value, setValue] = useState<CreatoResponse | null>(null);
  const [error, setError] = useState(null);

  const handleExecute = useCallback(() => {
    setPending(true);
    setValue(null);
    setError(null);
    return fetch('https://livus-frontend-test.s3.amazonaws.com/creator.json')
      .then(async response => setValue(await response.json()))
      .catch(error => setError(error))
      .finally(() => setPending(false));
  }, []);

  return { handleExecute, pending, value, error };
}

export { useCreator }
