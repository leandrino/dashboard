  
import React, { Suspense } from 'react'
import { Switch, Route } from 'react-router-dom'

const DashboardPage = React.lazy(() => import('./dashboard'));

export function AppRoute() {
    return (
      <Suspense fallback={<div>Carregando...</div>}>
        <Switch>
          <Route exact path="/" component={DashboardPage} />
          <Route path="/dashboard" component={DashboardPage} />
          <Route component={DashboardPage} />
        </Switch>
      </Suspense>
    )
}
