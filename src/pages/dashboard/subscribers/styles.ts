import styled from "styled-components"

const Wrapper = styled.section`
background-color: #FFFDFB;
min-height: 494px;
padding: 56px 0;
width: 100%;
`

export { Wrapper }
