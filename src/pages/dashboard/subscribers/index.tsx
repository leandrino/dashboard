import { useEffect } from "react";
import { useSubscribers } from "../../../hooks";
import { SubscribeList, Pagination } from "../../../components";
import { Wrapper } from "./styles";

const Subscribers = () => {
  const subscribers = useSubscribers();

  useEffect(() => {
    if (!subscribers.value) {
      subscribers.handleExecute();
    }
  }, [subscribers]);

  return (
    <Wrapper>
      {subscribers.error && (
        <h1>Algo de errado aconteceu, tente novamente mais tarde</h1>
      )}
      {subscribers.pending && <p>Carregando...</p>}
      {subscribers.value && (
        <>
          <SubscribeList subscribers={subscribers.value?.creator.subscribers} />
          <Pagination
            action={subscribers.changePage}
            dataList={[1, 2]}
            page={subscribers.page}
          />
        </>
      )}
    </Wrapper>
  );
};

export default Subscribers;
