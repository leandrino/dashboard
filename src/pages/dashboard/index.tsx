/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { useEffect } from 'react'
import { Header, Hero, Footer, Tabs } from '../../components'
import { useCreator } from '../../hooks'
import Subscribers from './subscribers'
import Classes from'./classes'

const Dashboard: React.FC = () => {
  const creator = useCreator()

  useEffect(() => {
    if (!creator.value) {
      creator.handleExecute()
    }

  }, [creator.value])

  if (creator.pending) {
    return <div>Carregando...</div>
  }

  if (creator.error) {
    return <div>Algo de errado aconteceu, tente novamente mais tarde!</div>
  }

  return (
    <>
      <Header />
      <Hero creatorData={creator.value} />
      <Tabs />
      <Switch>
        <Route path="/dashboard/subscribers" component={Subscribers} />
        <Route path="/dashboard/classes" component={Classes} />
      </Switch>
      <Footer />
    </>
  )
}

export default Dashboard
