import React from "react";
import { Subscriber } from "../../domains/subscriber";
import { Content, ContentRow } from "./styles";
import dayjs from 'dayjs';

interface SubscribeListProps {
  subscribers?: Subscriber[];
}

const SubscribeList: React.FC<SubscribeListProps> = ({ subscribers }) => {
  return (
    <>
      <Content>
        {subscribers?.map((subscriber) => {
          return (
            <ContentRow key={subscriber.id}>
              <p>{subscriber.name}</p>
              <p>{dayjs(subscriber.subscription_date).format('DD/MM/YYYY')}</p>
              <p>{subscriber.tier.name}</p>
            </ContentRow>
          );
        })}
      </Content>
    </>
  );
};

export { SubscribeList };
