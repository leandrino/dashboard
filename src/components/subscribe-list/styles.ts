import styled from 'styled-components'

const Content = styled.ul`
  list-style: none;
  width: 100%;

  @media screen and (min-width: 1140px) {
    margin: 0 auto;
    width: 1140px;
  }
`

const ContentRow = styled.li`
  align-items: center;
  border-top: 1px solid #AFAFAF;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-column-gap: 24px;
  font-size: 20px;
  font-weight: 400;
  justify-content: center;
  line-height: 32px;
  padding: 24px 0;

  &:last-child {
    border-bottom: 1px solid #AFAFAF;
  }

  & > p:nth-child(n) {
    text-align: center;
  }

  & > p:first-child {
    text-align: start;
  }

  & > p:last-child {
    text-align: end;
  }
`

export { Content, ContentRow }
