import styled from "styled-components";
import { Link as RRLink } from 'react-router-dom'


const Wrapper = styled.footer`
  background-color: #F6F6F4;
  height: 212px;
  width: 100%
`

const Container = styled.section`
  align-items: center;
  padding: 64px 20px;
  width: 100%;

  @media screen and (min-width: 1140px) {
    align-items: start;
    display: grid;
    grid-template-columns: 2fr 1fr 1fr 1fr 1fr;
    grid-column-gap: 24px;
    margin: 0 auto;
    padding: 64px 0;
    width: 1140px;
  }
`

const Title = styled.h3`
  color: #1A1A1A;
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;
  margin-bottom: 16px;
`

const Link = styled(RRLink)`
  color: #1A1A1A;
  font-size: 14px;
  text-decoration: none;
  width: 100%;
`

const ExternalLink = styled.a`
  color: #1A1A1A;
  font-size: 14px;
  text-decoration: none;
  width: 100%;
`

const List = styled.ul`
  list-style: none;

  & > li {
    margin-bottom: 16px;
  }
`

export { Wrapper, Container, Title, Link, ExternalLink, List }
