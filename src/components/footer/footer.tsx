import React from "react";
import { Wrapper, Container, Title, Link, ExternalLink, List } from "./styles";
import { Logo } from "../logo";

const Footer: React.FC = () => {
  return (
    <Wrapper>
      <Container>
        <Logo isBlack />
        <div>
          <Title>Comunidade</Title>
          <Link to="/creator-community">Creator Community</Link>
        </div>
        <div>
          <Title>Social</Title>
          <List>
            <li>
              <ExternalLink href="https://instagram.com/iamleandrino">
                Instagram
              </ExternalLink>
            </li>
            <li>
              <ExternalLink href="https://linkedin.com/in/lmechea">
                Linkedin
              </ExternalLink>
            </li>
          </List>
        </div>
        <div>
          <Title>Empresa</Title>
          <List>
            <li><Link to="/about">Sobre a Livus</Link></li>
            <li><Link to="/careers">Vagas</Link></li>
          </List>
        </div>
        <div>
          <Title>Ajuda</Title>
          <Link to="/help">Fale Conosco</Link>
        </div>
      </Container>
    </Wrapper>
  );
};

export { Footer };
