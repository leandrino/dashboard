import React from 'react'
import { Logo } from '../logo'
import { Wrapper, Container, Menu, Link } from './styles'

const Header: React.FC = () => {
  return (
    <Wrapper>
      <Container>
        <Logo />
        <Menu>
          <Link to='/price'>Preços</Link>
          <Link to='/dashboard'>Entrar</Link>
        </Menu>
      </Container>
    </Wrapper>
  )
}

export { Header }
