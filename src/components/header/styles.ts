import styled from "styled-components";
import { Link as RRLink } from 'react-router-dom'

const Wrapper = styled.header`
  background-color: #1A1A1A;
  color: #FFFFFF;
  width: 100%;
  `

const Container = styled.section`
  align-items: center;
  display: flex;
  height: 96px;
  justify-content: space-between;
  padding: 0 20px;
  width: 100%;

  @media screen and (min-width: 1140px) {
    margin: 0 auto;
    padding: 0;
    width: 1140px;
  }
`

const Menu = styled.div``

const Link = styled(RRLink)`
  color: #FFFFFF;
  font-size: 16px;
  font-weight: 500;
  text-decoration: none;

  &:not(last-child) {
    margin-left: 48px;
  }
`

export { Wrapper, Container, Menu, Link }
