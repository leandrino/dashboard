import styled from "styled-components";
import { Link as RRLink } from 'react-router-dom'

interface LinkProps {
  isActive?: boolean
}

const Wrapper = styled.header`
  border-bottom: 1px solid #000000; 
  color: #1A1A1A;
  width: 100%;
  `

const Container = styled.section`
  align-items: center;
  display: flex;
  height: 96px;
  justify-content: space-between;
  padding: 0 20px;
  width: 100%;

  @media screen and (min-width: 1140px) {
    margin: 0 auto;
    padding: 0;
    width: 1140px;
  }
`

const Menu = styled.div``

const Link = styled(RRLink)`
  color: #1A1A1A;;
  font-size: 24px;
  font-weight: 500;
  text-decoration: ${(props: LinkProps) => props.isActive ? 'underline' : 'none' };
  line-height: 32px;
  opacity: ${(props: LinkProps) => props.isActive ? '1' : '0.4' };


  &:not(:first-child) {
    margin-left: 48px;
  }
`

export { Wrapper, Container, Menu, Link }
