import React from "react";
import { useLocation } from "react-router-dom";
import { Wrapper, Container, Menu, Link } from "./styles";

const Tabs: React.FC = () => {
  const location = useLocation();

  return (
    <Wrapper>
      <Container>
        <Menu>
          <Link
            isActive={location.pathname === "/dashboard/subscribers"}
            to="/dashboard/subscribers"
          >
            Assinantes
          </Link>
          <Link
            isActive={location.pathname === "/dashboard/classes"}
            to="/dashboard/classes"
          >
            Cursos
          </Link>
        </Menu>
      </Container>
    </Wrapper>
  );
};

export { Tabs };
