import styled from "styled-components";

interface WrapperProps {
  background: string;
}

const Wrapper = styled.section<WrapperProps>`
  background-image: url("${(props) => props.background}");
  background-size: cover;
  height: 40vh;
  position: relative;
  width: 100%;

  @media screen and (min-width: 1140px) {
    height: 397px;
  }
`;

const ContentHeroOverlay = styled.div`
  background: linear-gradient(
    180deg,
    rgba(26, 26, 26, 0) 0%,
    rgba(26, 26, 26, 0.7) 100%
  );
  background-blend-mode: multiply;
  bottom: 0;
  left: 0;
  right: 0;
  position: absolute;
  top: 0;
`;

const ContentHero = styled.section`
  bottom: -16px;
  position: absolute;
  width: 100%;
`;

const Container = styled.section`
  align-items: center;
  display: flex;
  padding: 0 20px;
  width: 100%;

  @media screen and (min-width: 1140px) {
    margin: 0 auto;
    padding: 0;
    width: 1140px;
  }
`;

const Profile = styled.div<WrapperProps>`
  background-image: url("${(props) => props.background}");
  background-size: cover;
  border-radius: 4px;
  height: 170px;
  width: 170px;
`;

const ProfileInfo = styled.div`
  margin-left: 24px;
`;

const ProfileName = styled.h1`
  color: #fffdfb;
  font-size: 72px;
  font-weight: 500;
  margin-left: -8px;
`;

const ProfileHeadline = styled.p`
  color: #f6f6f4;
  font-size: 16px;
  line-height: 28px;
`;

export {
  Wrapper,
  ContentHero,
  Container,
  Profile,
  ContentHeroOverlay,
  ProfileInfo,
  ProfileName,
  ProfileHeadline,
};
