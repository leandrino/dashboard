import React from "react";
import { CreatoResponse } from "../../domains/creator";
import {
  Wrapper,
  ContentHero,
  Container,
  Profile,
  ContentHeroOverlay,
  ProfileInfo,
  ProfileName,
  ProfileHeadline,
} from "./styles";

interface HeroProps {
  creatorData: CreatoResponse | null;
}

const Hero: React.FC<HeroProps> = ({ creatorData }) => {
  const creator = creatorData?.creator;

  return (
    <Wrapper background={creator?.background_url || ""}>
      <ContentHeroOverlay />
      <ContentHero>
        <Container>
          <Profile
            background={creator?.photo_url || ""}
            aria-label={`Foto do perfil de ${creator?.name}`}
          />
          <ProfileInfo>
            <ProfileName>{creator?.name}</ProfileName>
            <ProfileHeadline>{creator?.headline}</ProfileHeadline>
          </ProfileInfo>
        </Container>
      </ContentHero>
    </Wrapper>
  );
};

export { Hero };
