import React from "react";
import { Content, Button } from "./styles";

interface PaginationProps {
  dataList: number[];
  action: (page: number) => void;
  page: number;
}

const Pagination: React.FC<PaginationProps> = ({ action, dataList, page }) => {
  return (
    <Content>
      {dataList.map((item) => {
        return (
          <Button isActive={page === item} key={item} onClick={(e) => {
            e.preventDefault()
            action(item)
          }}>
            {item}
          </Button>
        );
      })}
    </Content>
  );
};

export { Pagination };
