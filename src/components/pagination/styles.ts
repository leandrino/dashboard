import styled from "styled-components";

interface ButtonProps {
  isActive: boolean
}

const Content = styled.ul`
  align-items: center;
  display: flex;
  justify-content: center;
  list-style: none;
  padding: 24px 0 0;
  width: 100%;

  @media screen and (min-width: 1140px) {
    margin: 0 auto;
    width: 1140px;
  }
`

const Button = styled.button<ButtonProps>`
  background-color: ${props => props.isActive ? '#1A1A1A' : '#F6F6F4'};
  border: none;
  border-radius: 50%;
  color: ${props => props.isActive ? '#F6F6F4' : '#1A1A1A'};
  cursor: pointer;
  height: 32px;
  width: 32px;

  &:not(last-child) {
    margin-right: 10px;
  }
`

export { Content, Button }
