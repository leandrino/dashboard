# Project Dashboard

- [x] Header (1)
- [x] Hero (1)
- [-] Subscribers
  - [x] Tabs (1)
  - [ ] Route (1)
  - [ ] Subscribe List (2)
- [ ] Classes
  - [ ] Route (1)
  - [ ] Carousel (3)
  - [ ] Classes list (2)
- [x] Footer (1)
- [ ] Animations (2)
